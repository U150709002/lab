package esra.main;

import esra.shapes3d.Cube;
import esra.shapes3d.Cylinder;

public interface Test3D {
	
	public static void main(String[] args) {
		Cylinder cylinder = new Cylinder(6, 10);
		System.out.println(cylinder.toString());
		
		Cube cube = new Cube(5);
		System.out.println(cube.toString());
	}
		

}
