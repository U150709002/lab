
public class Int2Bin {
	
	public static void main (String[] args){
		System.out.println(int2bin(12));
	}
	  
	private static String int2bin(int number){
		if((number==0 || number==1)){
			return number + "" ;
			
		}
		return int2bin(number/2) + ( number % 2 );
		
	}
	
	

}
