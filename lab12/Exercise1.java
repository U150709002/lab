package generics;

import java.util.ArrayList;
import java.util.List;

public class Exercise1 {

	public static void main(String[] args) {
		String[] strs = {"a", "b", "c","d"};
		List<String> strList = new ArrayList<>();
		Exercise1.<String>addToCollection(strs,strList);
		
		Integer[] ints = {1, 2, 3, 4};
		List<Integer> intList = new ArrayList<>();
		Exercise1.<Integer>addToCollection(ints,intList);
		
		

	}
	public static <T> void addToCollection(T[] arr, List<T> list) {
		System.out.println("Generics Method");
		for ( T val: arr) {
			list.add(val);
		}
	}
	
	public static void addToCollection(String[] strs, List<String> strList) {
		System.out.println("String Method");
		for ( String str: strs) {
			strList.add(str);
		}
	}
//	public static void addToCollection(Integer ints, List<Integer> intList) {
//		for ( Integer i: ints) {
//			intList.add(ints);
//		}
//	}

}
