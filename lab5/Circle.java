
public class Circle {
	int radius ;
	
	public Circle(int radius){
		this.radius = radius;
	}
	
	public double area(){
		return /*Math.PI*/ 3.14 * radius * radius ;
	}
	
	public double perimeter(){
		return /*Math.PI*/ 3.14 * 2 * radius ;
	}
	
}
